# Computer Vision Test

This challenge required determining whether or not an image contained a person using Azure's Computer Vision API. 

## API 

There is a single API endpoint at `/api/analyze`. The endpoint receives a `multipart/form-data` request containing the image data, which will be forwarded onto the Azure API to detect the objects contained in the image. The API will process the response and return the confidence to the client.

The API finds all objects matching type "person" and determines if the highest confidence level is greater than 0.5. Originally, I used 0.75 but it appeared that it wasn't accurate enough. Combining both the check for relevant objects with a simple check against the highest confidence seemed to be the most accurate way to determine if the image was a person.

Data is returned in the following format:

```js
{
	"status": "success", // success or failed
	"data": {
		"total": 1, // total objected detected
		"avg": 0.647, // average confidence of all relevant objects
    "max": 0.647 // maximum confidence of relevant objects
    "confident": true // whether or not the API is confident the object matches
	}
}
```

## Setup

The application runs by default at `http://127.0.0.1:3000`. There is a _very_ (vanilla JavaScript, no framework) simple web interface at `http://127.0.0.1:3000/index.html` once the app is started.

```bash
$ npm install # install dependencies

$ cp .env.example .env # edit the .env file and configure API key and endpoint

$ npm run start # start the application
```

Once the app is running you can visit the web interface to test the image upload and processing. 

## Tests

I also included some unit and feature tests to make up for the minimal interface. The tests use sample images located in `test/support` The tests can be run as such:

```bash
$ npm run test
```
