import axios from 'axios'
import { DetectedObject, ObjectDetectionResult } from '../types'

export class ObjectDetectionService {
  private key: string
  private endpoint: string

  public constructor (key: string, endpoint: string) {
    this.key = key
    this.endpoint = endpoint
  }

  /**
   * Use Azure's Object Detection Computer Vision API to find objects matching a given
   * type in an image. Returns the matched objects, several statistics calculated based
   * on the API results, and whether or not the minimum threshold for overall confidence
   * is met.
   *
   * @param {Buffer} buffer the image data
   * @param {string} type the type of object to search for
   * @param {number} threshold the minimum threshold for confidence in a matched object
   * @return {Promise<ObjectDetectionResult>}
   */
  public async detectObjects (buffer: Buffer, type: string, threshold: number = 0.5): Promise<ObjectDetectionResult> {
    const headers = {
      'Ocp-Apim-Subscription-Key': this.key,
      'Content-Type': 'application/octet-stream',
    }

    // make azure object detection API call
    const { data: { objects } } = await axios.post(this.apiUrl, buffer, { headers })

    // find objects that match the requested type, sort descending by confidence
    const relevant = (objects as DetectedObject[]).filter(o => o.object === type).sort((a, b) => b.confidence - a.confidence)

    // calculate a few relevant statistics
    const total = relevant.length
    const avg = total > 0 ? relevant.reduce((carry, p) => carry + p.confidence, 0) / total : 0
    const max = relevant[0]?.confidence ?? 0
    const confident = max > threshold

    return { objects: relevant, total, avg, max, confident }
  }

  get apiUrl () {
    return `${this.endpoint}/vision/v3.0/detect`
  }
}
