export interface DetectedObject {
  rectangle: Rectangle
  object: string
  confidence: number
}

export interface Rectangle {
  x: number
  y: number
  w: number
  h: number
}

export interface ObjectDetectionResult {
  objects: DetectedObject[]
  total: number
  avg: number
  max: number
  confident: boolean
}
