import express from 'express'
import multer from 'multer'
import { Server } from 'http'
import { ObjectDetectionService } from './services/object-detection.service'

require('dotenv').config()

const app = express()
const upload = multer()

app.use('/', express.static('public'))

let server: Server = null

const API_KEY = process.env.AZURE_API_KEY
const API_ENDPOINT = process.env.AZURE_API_ENDPOINT

if (!(API_ENDPOINT && API_KEY)) {
  console.error('The API key and endpoint are not configured. Copy the .env.example file and enter the values.')
  process.exit(1)
}

console.log('Loaded configuration....')
console.log(`API Key: ${API_KEY}`)
console.log(`API Endpoint: ${API_ENDPOINT}`)

const detector = new ObjectDetectionService(API_KEY, API_ENDPOINT)

app.post('/api/analyze', upload.single('file'), async (req, res) => {
  const file = req.file

  try {
    // could make this detect any object type by changing the second parameter!
    const { total, avg, max, confident } = await detector.detectObjects(file.buffer, 'person')

    console.log(`Processed image ${file.filename}, finding ${total} relevant objects with a maximum confidence of ${max} and an average of ${avg}.`)

    return res.status(200).json({ status: 'success', data: { total, avg, max, confident } })
  } catch (e) {
    console.error(e)
    return res.status(400).json({ status: 'failed' })
  }
})

export const start = (port: number = 3000, host: string = '127.0.0.1') => {
  return server = app.listen(port, host, () => {
    console.log(`Server listening at http://${host}:${port}`)
  })
}

export const stop = () => {
  return server.close()
}
