import fs from 'fs'
import path from 'path'
import { ObjectDetectionService } from '../../src/services/object-detection.service'

require('dotenv').config()

const API_KEY = process.env.AZURE_API_KEY
const API_ENDPOINT = process.env.AZURE_API_ENDPOINT

const FILES = [
  { file: 'meeting.jpg', expected: true },
  { file: 'skateboarder.jpg', expected: true },
  { file: 'postit.jpg', expected: false },
  { file: 'bike.jpg', expected: false },
]

describe('Object Detection Service', () => {
  const detector = new ObjectDetectionService(API_KEY, API_ENDPOINT)

  it('should be detect determine the correct result', async () => {
    for (const { file, expected } of FILES) {
      const location = path.join(__dirname, '..', 'support', file)
      const buffer = fs.readFileSync(location)

      const { confident } = await detector.detectObjects(buffer, 'person')

      expect(confident).toEqual(expected)
    }
  })
})
