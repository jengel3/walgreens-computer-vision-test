import FormData from 'form-data'
import axios from 'axios'
import path from 'path'
import fs from 'fs'
import { start, stop } from '../../src/server'

axios.defaults.baseURL = 'http://127.0.0.1:3000'

beforeAll(() => {
  // start express server
  start()
})

afterAll(() => {
  // close express server
  stop()
})

const FILES = [
  { file: 'meeting.jpg', expected: true },
  { file: 'skateboarder.jpg', expected: true },
  { file: 'postit.jpg', expected: false },
  { file: 'bike.jpg', expected: false },
]

describe('Object Detection API: /api/analyze', () => {
  it('should return the expected result', async () => {
    for (const { file, expected } of FILES) {
      const location = path.join(__dirname, '..', 'support', file)
      const buffer = fs.createReadStream(location)

      const formData = new FormData()
      formData.append('file', buffer, file)

      const { data: { data: { confident } } } = await axios.post('/api/analyze', formData, { headers: formData.getHeaders() })

      expect(confident).toEqual(expected)
    }
  })
})
